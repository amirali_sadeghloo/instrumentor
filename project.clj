(defproject instrumentor "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/spec.alpha "0.2.176"]

                 [io.pedestal/pedestal.service       "0.5.7"]
                 [io.pedestal/pedestal.service-tools "0.5.7"]
                 [io.pedestal/pedestal.route         "0.5.7"]
                 [io.pedestal/pedestal.jetty         "0.5.7"]

                 [ragtime "0.7.2"]
                 [org.clojure/java.jdbc "0.6.1"]
                 [org.postgresql/postgresql "42.2.5"]
                 [com.mchange/c3p0 "0.9.5.2"]
                 [clojure.java-time "0.3.2"]
                 [com.novemberain/langohr "5.0.0"]
                 [com.stuartsierra/component "0.4.0"]
                 [nilenso/honeysql-postgres "0.2.6"]

                 [buddy/buddy-auth "2.1.0" :exclusions [cheshire]]
                 [buddy/buddy-hashers "1.4.0"]

                 [com.climate/claypoole "1.1.4"]
                 [com.mchange/c3p0 "0.9.5.2"]
                 [stellar.lib/schema "0.4.0-SNAPSHOT"]
                 [org.clojure/core.async "0.4.474"]
                 [com.taoensso/timbre "4.10.0"]
                 [cheshire "5.8.0"]
                 [aero "1.1.3"]
                 [com.taoensso/nippy "2.14.0"]
                 [org.clojure/spec.alpha "0.2.176"]]
  :main ^:skip-aot instrumentor.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
