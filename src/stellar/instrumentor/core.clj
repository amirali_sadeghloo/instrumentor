(ns stellar.instrumentor.core
  (:gen-class)
  (:require [com.stuartsierra.component :as component]
            [aero.core :as aero]
            [taoensso.timbre :as timbre :refer [spy]]
            [stellar.instrumentor.components.web.core :as web]
            [stellar.instrumentor.components.web.service-map :as wep]
            [stellar.instrumentor.components.instrument.core :as inore]

            [stellar.instrumentor.components.instrument.config :as infig]
            [stellar.instrumentor.components.db.protocol :as dbtocol]
            [stellar.instrumentor.components.db.postgres.conn-pool :as pool]
            [stellar.instrumentor.components.connection.core :as amqore]
            [stellar.instrumentor.components.connection.config :as amqfig]
            [stellar.instrumentor.components.db.postgres.core :as db]
            [stellar.instrumentor.components.db.postgres.config :as dbfig]))

(defn system
  [db-config web-config instrumentor-config amqp-config]

  (merge
   (dbfig/subsystem  db-config)
   (infig/subsystem instrumentor-config)
   (amqfig/subsystem amqp-config)
   (wep/subsystem web-config)

   (db/subsystem)
   (amqore/subsystem)
   (web/subsystem)
   (inore/subsystem)))


;;----------------------------------------------------------------------------;;

(defn single-round-env-setup
  "Migrates the database and creates the
  superuser aliamidi"
  [db-config migrations-path & mig-ids]
  (timbre/log :info "Setting up DB for test ...")
  (apply pool/migrate db-config migrations-path mig-ids)
  (timbre/log :info "Setting up DB for test ... DONE !"))

;;----------------------------------------------------------------------------;;

(defn single-round-env-teardown
  "Drops all tables to returns an empty DB
  at the end of each round of test"
  [db-config migrations-path & mig-ids]
  (timbre/log :info "Setting up DB for test ...")
  (apply pool/rollback db-config migrations-path mig-ids)
  (timbre/log :info "Setting up DB for test ... DONE !"))

;;----------------------------------------------------------------------------;;
;;----------------------------------------------------------------------------;;

(defn -main
  [config-path]
  (let [config              (aero/read-config config-path)
        db-config           (:db config)
        web-config          (:web config)
        amqp-config         (:amqp config)
        instrumentor-config (:instrumentor config)
        sys                 (system db-config web-config
                                    instrumentor-config amqp-config)
        started-sys         (-> sys (component/start))]))

;;----------------------------------------------------------------------------;;
;;----------------------------------------------------------------------------;;

(comment

  (def config (aero/read-config "resources/config/config.edn"))
  (def db-config (:db config))
  (def web-config (:web config))
  (def instrumentor-config (:instrumentor config))
  (def amqp-config (:amqp config))

  (def sys (system db-config web-config instrumentor-config amqp-config))

  (def started-sys (-> sys (component/start)))
  (def stopped-sys (-> started-sys (component/stop)))

  (def db (:db started-sys))

  (single-round-env-setup db-config "resources/migrations/" "001-initial")

  (single-round-env-teardown db-config "resources/migrations/" "001-initial")

  )

;; java.sql.BatchUpdateException: Batch entry 0 INSERT INTO "instrument" VALUES ("description", 'بانک تجارت'), ("marketCode", 'GROUP_F2'), ("updated-at", NULL), ("maxOrderCount", 10000), ("schema-time-instrument", NULL), ("updatedAt", NULL), ("freeFloat", 0.0), ("minOrderCount", 1), ("name", 'طجار30182'), ("marketUnit", NULL), ("marketType", 'STOCK'), ("sector", '{:code "57", :name "بانکها و موسسات اعتباری", :superSectorCode "0001"}'), ("createdAt", NULL), ("marketName", 'بازار جبرانی آتی'), ("sub-sector", ''), ("tseId", 1960177179877308), ("id", 'IROFBTEJ3392'), ("totalShares", 1000), ("created-at", NULL), ("englishName", '3B392'), ("subSector", (  )), ("validity", 'NOT_VALID'), ("board", '{:code "8", :name "ابزار مشتقه"}'), ("baseVolume", 1) was aborted: ERROR: syntax error at or near ")"
