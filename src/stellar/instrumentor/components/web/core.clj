(ns stellar.instrumentor.components.web.core
  (:require [clojure.core.async :as async]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [com.stuartsierra.component :as component]
            [taoensso.timbre :as timbre]

            [stellar.instrumentor.components.web.server.helpers :as pers]
            [stellar.instrumentor.components.web.service-map    :as wep]
            [stellar.instrumentor.components.web.server.routing :as routing]))


;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;

(defrecord Web-Module [web-config service db]

  component/Lifecycle

  (start [component]
    (let [service-map (wep/get-service-map web-config)]

      (timbre/spy :info ["Starting Web Module" service-map])
      (if service
        component
        (cond-> service-map
          true                           http/create-server
          (not (pers/test? service-map)) http/start
          true                           ((partial assoc component :service))))))

  (stop [component]
    (timbre/spy :info ["Stopping Web Module" web-config])
    (when (and service (not (pers/test? web-config)))
      (http/stop service))
    (assoc component :service nil)))

;;------------------------------------------------------------------;;

(defn subsystem
  []
  (component/system-map
   :web (-> (map->Web-Module {})
            (component/using {:db :db})
            (component/using {:web-config :web-config}))))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;
