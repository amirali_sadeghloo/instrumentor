(ns stellar.instrumentor.components.web.server.helpers)

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;

(defn test?
  [service-map]
  (= :test (:env service-map)))

(defn service-fn
  "Extracts the service function which is called by the servlet
  container whenever a request is arrived.
  It is supposed to be used in tests"
  [system]
  (get-in system [:web :service :io.pedestal.http/service-fn]))


;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;
