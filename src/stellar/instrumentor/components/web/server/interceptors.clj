(ns stellar.instrumentor.components.web.server.interceptors
  (:require [taoensso.timbre :as timbre]
            [io.pedestal.http.content-negotiation :as contneg]
            [cheshire.core]

            [stellar.instrumentor.components.db.protocol :as dbtocol] ))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;

(defn accepted-type
  [context]
  (get-in context [:request :accept :field] "text/plain"))

(defn transform-content
  [body content-type]
  (case content-type
    "application/edn"  (pr-str body)
    "application/json" (cheshire.core/encode body)))

(defn coerce-to
  [response content-type]
  (-> response
      (update :body transform-content content-type)
      (assoc-in [:headers "Content-Type"] content-type)))

(def coerce-body
  {:name ::coerce-body
   :leave
   (fn [context]
     (-> context
      (update-in [:response] coerce-to (accepted-type context))))})

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;

(def supported-types
  ["application/edn" "application/json"])

(def content-neg-intc
  (contneg/negotiate-content
   supported-types
   {:no-match-fn (fn [context]
                   (throw (Exception. "Content Negotiation : No Match!")))}))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;

(defn- extract-params
  [request]
  (case (:content-type request)
      "application/json" (:json-params request)
      "application/edn"  (:edn-params request)))

(defn get-params
  [context]
  (:extracted-params context))

(def params-ceptor
  {:name ::params-extraceptor
   :enter (fn [context]
            (let [content-type (get-in context
                                       [:request :content-type])]
              (cond-> context
                (= content-type "application/json")
                (assoc :extracted-params
                       (extract-params (:request context)))

                (= content-type "application/edn")
                (assoc :extracted-params
                       (extract-params (:request context)))
                )))})

(defn +result
  [context result]
  (assoc-in context [:request :result] result))

(defn get-result
  [context]
  (get-in context [:request :result]))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;


(defn response-ok
  [unalterated-body]
  {:status 200
   :body unalterated-body})

(def general-result-ceptor
  {:name  ::general-result-handler
   :enter (fn [context]
            (-> context
                (assoc :response
                       (response-ok
                        (get-result context)))))})

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;

(defn <-instrument-name-ceptor
  [db]
  :name ::<-instrument-name-ceptor
  :enter
  (fn <-instrument-name
    [context]
    (try
      (let [isin (-> context
                     (:extracted-params)
                     (:isin))
            result (dbtocol/<-name-by-isin db isin)]
        (-> context
            (+result result)))
      (catch Exception e
        (timbre/spy :info "<-name-by-isin failed!")
        (.printStackTrace e)))))

(defn <-instrument-ceptor
  [db]
  :name ::<-instrument-ceptor
  :enter
  (fn <-instrument
    [context]
    (try
      (let [isin (-> context
                     (:extracted-params)
                     (:isin))
            result (dbtocol/<-instrument db isin)]
        (-> context
            (+result result)))
      (catch Exception e
        (timbre/spy :info "<-instrument failed!")
        (.printStackTrace e)))))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;
