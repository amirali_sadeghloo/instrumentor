(ns stellar.instrumentor.components.web.server.routing
  (:require [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [io.pedestal.http.route.path :as path]
            [taoensso.timbre :as timbre]

            [stellar.instrumentor.components.web.server.interceptors :as ceptors]))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;

(def common-routing-prefix
  [ceptors/coerce-body
   ceptors/content-neg-intc
   (body-params/body-params)
   ceptors/params-ceptor])

(def common-routing-suffix
  [ceptors/general-result-ceptor])

(defn common-route
  [& middle-interceptors]
  (-> (concat common-routing-prefix
              (-> middle-interceptors (into []))
              common-routing-suffix)
      (vec)))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;

(defn routing-scheme
  [db]
  {:pre [(some? db)]}

  #{["/instrument-name" :get (common-route
                              (ceptors/<-instrument-name-ceptor db))
     :route-name ::get-instrument-name]

    ["/whole-instrument" :get (common-route
                               (ceptors/<-instrument-ceptor db))
     :route-name ::get-instrument]})

;;------------------------------------------------------------------;;

(defn routes
  [db]
  (route/expand-routes
   (routing-scheme db)))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;
