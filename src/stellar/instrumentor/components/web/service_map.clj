(ns stellar.instrumentor.components.web.service-map
  (:require [buddy.core.keys :as keys]
            [com.stuartsierra.component :as component]
            [stellar.instrumentor.components.web.server.routing :as routing]
            [taoensso.timbre :as timbre]))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;

(defrecord Web-Config [service-map db]

  component/Lifecycle
  (start [component]
    (-> component
        (assoc-in [:service-map :io.pedestal.http/routes]
                  (routing/routes db ))))
  (stop [component]
    (-> component
        (assoc-in [:service-map :io.pedestal.http/routes]
                  nil))))

;;------------------------------------------------------------------;;

(defn subsystem
  "Create configuration map that gets passed to
  web/subsystem.
  arguments
  ----
  `:env`                    : #{:test ,,,}
  `:servlet-container`      : #{:jetty :tomcat ,,,}
  `:port`                   : int
  ----
  If the env is set to test the start wont
  actually start a web server"
  [{:keys [env port servlet-container host]
    :as pre-config}]

  {:pre [(some? env)
         (some? port)
         (some? servlet-container)]}

  (let [service-component (->> {:io.pedestal.http/port   port
                                :io.pedestal.http/join?  false
                                :io.pedestal.http/host   host
                                :io.pedestal.http/type   servlet-container}
                               (hash-map :service-map)
                               (map->Web-Config))]
    (component/system-map
     :web-config (-> service-component
                     (component/using {:db :db})))))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;

(defn get-service-map
  [web-config-component]
  (-> web-config-component (:service-map)))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;
