(ns stellar.instrumentor.components.db.postgres.conn-pool
  (:require
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.java.jdbc :as jdbc]
            [clojure.core.async :as async]

            [ragtime.jdbc]
            [ragtime.repl]
            [ragtime.core]

            [honeysql.core :as sql]
            [honeysql.helpers          :refer [insert-into columns values join left-join
                                               select where from order-by limit]]
            [honeysql-postgres.helpers :refer [upsert on-conflict do-update-set returning]]
            [honeysql-postgres.format]

            [com.stuartsierra.component :as component]

            [taoensso.timbre :as timbre])
  (:import (com.mchange.v2.c3p0 ComboPooledDataSource)))

;;------------------------------------------------------------------;;
;; database configuration
;;------------------------------------------------------------------;;

(defn- load-ragtime-config
  [config migrations-path]
  {:datastore (ragtime.jdbc/sql-database config)
   :migrations (ragtime.jdbc/load-resources migrations-path)})

(defn migrate'
  "Load migrations from `migration-path` and migrates the migrations
  corresponding to migration keys specified as `to-migrate-keys` arg
  on the db specified with `db-config`. Finally it reports back the list of
  migration keys currently that are currently applied on the db.
  NOTE :: the migration key for a file named as '002-initial.up.sql' is
  `002-initial.up`"
  [db-config migrations-path & to-migrate-keys]
  {:pre [(not (empty? to-migrate-keys))]}
  (let [migratable-db    (ragtime.jdbc/sql-database db-config)
        migrations       (ragtime.jdbc/load-resources migrations-path)
        migrations-index (ragtime.core/into-index migrations)]
    (doseq [mig-key to-migrate-keys]
      (ragtime.core/migrate migratable-db (get migrations-index mig-key)))))

(defn migrate
  "Load migrations from `migration-path` and migrates the migrations
  corresponding to migration keys specified as `to-migrate-keys` arg
  on the db specified with `db-config`. Finally it reports back the list of
  migration keys currently that are currently applied on the db.
  NOTE :: the migration key for a file named as '002-initial.up.sql' is
  `002-initial.up`"
  [db-config migrations-path & to-migrate-keys]
  {:pre [(not (empty? to-migrate-keys))]}
  (let [migratable-db    (ragtime.jdbc/sql-database db-config)
        migrations       (ragtime.jdbc/load-directory migrations-path)
        migrations-index (ragtime.core/into-index migrations)]
    (doseq [mig-key to-migrate-keys]
      (ragtime.core/migrate migratable-db (get migrations-index mig-key)))))

(defn rollback
  "Load migrations from `migration-path` and rollbacks the migrations
  corresponding to the keys specified as `to-rollback-keys` arg
  on the db specified with `db-config`.Finally it reports back the list of
  migration keys currently that are currently applied on the db.
  NOTE :: the key for a migration file named as '002-initial.up.sql' is
  `002-initial.up`"
  [db-config migrations-path & to-rollback-keys]
  (let [migratable-db    (ragtime.jdbc/sql-database db-config)
        migrations       (ragtime.jdbc/load-directory migrations-path)
        migrations-index (ragtime.core/into-index migrations)]
    (doseq [mig-key to-rollback-keys]
      (ragtime.core/rollback migratable-db (get migrations-index mig-key)))
    (map :id (ragtime.core/applied-migrations migratable-db migrations-index))))

;;------------------------------------------------------------------;;
;; connection pool
;;------------------------------------------------------------------;;

(defmacro ^:private resolve-new
  [class]
  (when-let [resolved (resolve class)]
    `(new ~resolved)))

(defn- as-properties
  [m]
  (let [p (java.util.Properties.)]
    (doseq [[k v] m]
      (.setProperty p (name k) (str v)))
    p))

(defn- pool
  [{:keys [subprotocol
           subname
           classname
           excess-timeout
           idle-timeout
           initial-pool-size
           minimum-pool-size
           maximum-pool-size
           test-connection-query
           idle-connection-test-period
           test-connection-on-checkin
           test-connection-on-checkout]
    :or {excess-timeout (* 30 60)
         idle-timeout (* 3 60 60)
         initial-pool-size 3
         minimum-pool-size 3
         maximum-pool-size 15
         test-connection-query nil
         idle-connection-test-period 0
         test-connection-on-checkin false
         test-connection-on-checkout false}
    :as spec}]
  {:datasource
   (doto (resolve-new ComboPooledDataSource)
                 (.setDriverClass classname)
                 (.setJdbcUrl (str "jdbc:" subprotocol ":" subname))
                 (.setProperties (as-properties
                                  (dissoc spec
                                          :classname
                                          :subprotocol
                                          :subname
                                          :naming
                                          :delimiters
                                          :alias-delimiter
                                          :excess-timeout
                                          :idle-timeout
                                          :initial-pool-size
                                          :minimum-pool-size
                                          :maximum-pool-size
                                          :test-connection-query
                                          :idle-connection-test-period
                                          :test-connection-on-checkin
                                          :test-connection-on-checkout)))
                 (.setMaxIdleTimeExcessConnections excess-timeout)
                 (.setMaxIdleTime idle-timeout)
                 (.setInitialPoolSize initial-pool-size)
                 (.setMinPoolSize minimum-pool-size)
                 (.setMaxPoolSize maximum-pool-size)
                 (.setIdleConnectionTestPeriod idle-connection-test-period)
                 (.setTestConnectionOnCheckin test-connection-on-checkin)
                 (.setTestConnectionOnCheckout test-connection-on-checkout)
                 (.setPreferredTestQuery test-connection-query))})

(defn close-pool!
  [{{:keys [^ComboPooledDataSource datasource]} :pool}]
  (try
    (doto datasource
      (.close))
    (catch Exception e
      (throw
       (ex-info "close-pool!"
                {:cause e
                 :params {:pool datasource}})))))

(defn create-pool!
  [config]
  (timbre/spy "trying to add a pool")
  (try
    {:pool (-> config (pool))
     :options {:naming {:fields identity :keys identity}
               :delimiters ["\"" "\""]
               :alias-delimiter " AS "}}
    (catch Exception e
      (throw
       (ex-info "create-pool!"
                {:cause e
                 :params {:config config}})))))

(defn get-connection
  [{:keys [pool]}]
  (try
    (->> pool)
    (catch Exception e
      (throw
       (ex-info "get-connection"
                {:cause e
                 :params {:pool pool}})))))

;;------------------------------------------------------------------;;
;; Query and Transaction Facilities
;;------------------------------------------------------------------;;

(defn format-sql
  [sql-map & params]
  (try
    (apply
     sql/format
     sql-map
     (conj
      (into [] params)
      :quoting :ansi
      :allow-dashed-names? true))
    (catch Exception e
      (throw
       (ex-info "format-sql"
                {:cause e
                 :params {:sql-map sql-map
                          :params params}})))))

(defn- transact!-
  [conn & operations]
  (jdbc/with-db-transaction [conn conn]
    (doseq [op operations]
      (jdbc/execute! conn (format-sql op)))))

(defn transact!
  [access & operations]
  (try
    (apply transact!- (get-connection access) operations)
    (catch Exception e
      (throw
       (ex-info "transact!"
                {:cause e
                 :params {:access access}})))))

;;------------------------------------------------------------------;;

(defn transact-with-returning!
  [access & ops-fns]
  (jdbc/with-db-transaction [conn (get-connection access)]
    (try
      (loop [op-fn   ((first ops-fns))
             rmn-ops (rest ops-fns)]
        (let [last-val (first (jdbc/query conn (format-sql op-fn)))]
          (if (> (count rmn-ops) 0)
            (recur ((first rmn-ops) last-val)
                   (rest rmn-ops)))))
      (catch Exception e
        (throw
         (ex-info "transact-with-returning!"
                  {:cause e
                   :params {:ops-fns ops-fns
                            :connection conn}}))))))

;;------------------------------------------------------------------;;

(defn query!-
  [conn query]
  (jdbc/query conn (format-sql query)))

(defn execute!-
  [conn query]
  (jdbc/execute! conn (format-sql query)))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;
