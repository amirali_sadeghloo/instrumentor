(ns stellar.instrumentor.components.db.postgres.config
  (:require [com.stuartsierra.component :as component]))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;

(defn subsystem
  "Supplies the system with a statelss
  configuration map for db Component
  TODO :: add spec and validation"
  [{db-type :type :as db-config}]
  (component/system-map
   :db-config (case db-type
                :postgres (->> db-config))))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;
