(ns stellar.instrumentor.components.db.postgres.core
  (:require
   [com.stuartsierra.component :as component]
   [taoensso.timbre :as timbre]
   [taoensso.nippy :as nippy]
   [clojure.java.jdbc :as jdbc]

   [stellar.instrumentor.components.db.protocol :as dbtocol]
   [stellar.instrumentor.components.db.postgres.config :as db-conf]
   [stellar.instrumentor.components.db.postgres.conn-pool :as gool]
   [stellar.instrumentor.components.db.postgres.write :as pite]
   [stellar.instrumentor.components.db.postgres.read :as pead])
  (:import
   (com.mchange.v2.c3p0 ComboPooledDataSource)))

(defrecord PostgresDB [db-config access]

  ;;--------------------------------------------------------;;
  component/Lifecycle
  ;;--------------------------------------------------------;;

  (start [db]
    (timbre/spy :info ["Starting DB ..." db-config])
    (->> (timbre/spy :info (gool/create-pool! db-config))
         (assoc db :access)))

  (stop [db]
    (timbre/spy :info ["Stopping DB ..." db-config])
    (case  (not (some? access))
      true  (->> db)
      false (do (dbtocol/close-pool! db)
                (assoc db :access nil))))

  ;;--------------------------------------------------------;;
  dbtocol/Pooling
  ;;--------------------------------------------------------;;

  (create-pool!
    [db]
    (gool/create-pool! db-config))

  (close-pool!
    [db]
    (-> access
        (gool/close-pool!)))

  (transact! [db conn-requiring-fn]
    (jdbc/with-db-transaction
      [conn (dbtocol/get-connection db)]
      (conn-requiring-fn conn)))

  (read-only-transact! [db conn-requiring-fn]
    (jdbc/with-db-transaction
      [conn (dbtocol/get-connection db) {:read-only? true}]
      (conn-requiring-fn conn)))


  (query [db query]
    (jdbc/query (dbtocol/get-connection db)
                (gool/format-sql query)))

  (get-connection
    [db]
    (gool/get-connection access))

  ;;--------------------------------------------------------;;
  dbtocol/Read
  ;;--------------------------------------------------------;;

  (<-name-by-isin
    [db isin]
    (let [conn (dbtocol/get-connection db)]
      (timbre/spy conn)
      (pead/instrument-name-by-isin conn isin)))

  (<-instrument [db isin]
    (let [conn (dbtocol/get-connection db)]
      (timbre/spy conn)
      (pead/instrument-by-isin conn isin)))

  ;;--------------------------------------------------------;;
  dbtocol/Write
  ;;--------------------------------------------------------;;

  (->instrument
    [db instrument]
    (let [conn (dbtocol/get-connection db)]
      (pite/persist-instrument conn instrument))))
;;------------------------------------------------------------------;;

(defn subsystem
  []
  (component/system-map
   :db (-> (map->PostgresDB {})
           (component/using {:db-config :db-config}))))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;
