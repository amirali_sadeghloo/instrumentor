(ns stellar.instrumentor.components.db.postgres.read
  (:require
   [honeysql.helpers :as honey]
   [stellar.instrumentor.components.db.postgres.conn-pool :as gool]))

(defn query:-instrument-name-by-isin
  [isin]
  (-> (honey/select :instrument-name)
      (honey/from :instrument)
      (honey/where [:= :isin isin])))

(defn instrument-name-by-isin
  [conn isin]
  (-> query:-instrument-name-by-isin isin)
  (->> (gool/query!- conn)
       (first)))

(defn query:-instrument-by-isin
  [isin]
  (-> (honey/select :*)
      (honey/from :instrument)
      (honey/where [:= :isin isin])))

(defn instrument-by-isin
  [conn isin]
  (-> (query:-instrument-by-isin isin)
      (->> (gool/query!- conn)
           (first))))


