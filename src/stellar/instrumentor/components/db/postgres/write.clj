(ns stellar.instrumentor.components.db.postgres.write
  (:require
   [honeysql-postgres.helpers
    :refer
    [do-update-set on-conflict returning upsert]]
   [taoensso.nippy :as nippy]

   [clj-time.coerce :as time]

   [honeysql.core :as sql]
   [honeysql.helpers :as honey]
   [honeysql-postgres.helpers :as psqlh]
   [taoensso.timbre :as timbre]

   [stellar.instrumentor.components.db.postgres.conn-pool
    :as gool]))
(defn to-timestamp [some-key]
  (time/to-timestamp some-key))


(defn change-datetime [map name]
  (if (get map name)
    (assoc map name (time/to-timestamp (get map name)))
    map))

;;------------------------------------------------------------------;;

(defn persistable [instrument]
  (-> instrument
      (select-keys [:instrument-id :type :isin :instrument-name
                    :english-name :description :tse-id :state
                    :market-name :market-code :num-of-paths-insert
                    :num-of-paths-ins :num-of-paths :instrument-ins
                    :trading-status-time-table-ins :free-float
                    :max-order-count :min-order-count :total-shares
                    :base-volume :validity :sector :sub-sector
                    :board :marketUnit :marketType :updated-at
                    :created-at :instrument-timestamp
                    :schema-time-instrument])
      (assoc :sector (str (:sector instrument)))
      (assoc :sub-sector (str (:sub-sector instrument)))
      (assoc :board (str (:board instrument)))
      (change-datetime :updated-at)
      (change-datetime :created-at)
      (change-datetime :schema-time-instrument)
      (update :type name)))

;;------------------------------------------------------------------;;

(defn persist-instrument
  [conn instrument]
  (try
    (-> (honey/insert-into :instruments)
        (honey/values [(persistable instrument)])
        (->> (gool/execute!- conn)))
    (catch Exception e
      (.printStackTrace e))))


