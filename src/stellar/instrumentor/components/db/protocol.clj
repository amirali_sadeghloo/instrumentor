(ns stellar.instrumentor.components.db.protocol)


(defprotocol Pooling

  (create-pool!        [db])
  (close-pool!         [db])
  (get-connection      [db])
  (query               [db query])
  (transact!           [db conn-requiring-fn])
  (read-only-transact! [db conn-requiring-fn]))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;

(defprotocol Read

  (<-name-by-isin  [db isin])
  (<-instrument [db isin]))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;

(defprotocol Write

  (->instrument           [db instrument]))

