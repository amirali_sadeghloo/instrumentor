(ns stellar.instrumentor.components.connection.config
  (:require [com.stuartsierra.component :as component]))

(defn subsystem
  [amqp-config]
  (component/system-map
   :amqp-config amqp-config))
