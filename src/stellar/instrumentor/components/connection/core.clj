(ns stellar.instrumentor.components.connection.core
  (:require
   [com.stuartsierra.component :as component]
   [stellar.instrumentor.components.connection.reader :as conread]
   [clojure.core.async :as async]
   [stellar.lib.schema.bixo.transform.core :as brore]
   [taoensso.timbre :as timbre]))



(defrecord Amqp-connection
    [instrument-reader

     amqp-config]

  ;;----------------------------------------------------------------;;
   component/Lifecycle
  ;;----------------------------------------------------------------;;

  (start [component]
    (timbre/spy :info ["Connection is about to be started ..."])
    (case
        (nil? instrument-reader)

      true (let [reader-conf       (-> amqp-config (assoc :decoder brore/rlc-deserialcoder))
                 instrument-reader (conread/reader reader-conf)]
             (-> component
                 (assoc :instrument-reader
                        instrument-reader)))
      false (-> component)))

  (stop [component]
    (when (some? instrument-reader)
      (async/go
        (async/>! (:close-ch instrument-reader) "Close Connection"))
      (timbre/spy :info ["Connection is about to be stopped..."]))))

;;----------------------------------------------------------------;;
(defn subsystem
  []
  (component/system-map
   :amqp (-> (map->Amqp-connection {})
             (component/using [:amqp-config]))))


;; (-> amqp-config (assoc :decoder brore/rlc-deserialcoder))
