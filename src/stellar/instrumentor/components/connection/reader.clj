(ns stellar.instrumentor.components.connection.reader
  (:require [langohr.core      :as rmq]
            [langohr.channel   :as lch]
            [langohr.queue     :as lq]
            [langohr.consumers :as lc]
            [clojure.spec.alpha :as spec]
            [langohr.basic     :as lb]
            [langohr.exchange  :as le]
            [clojure.core.async :as async]
            [cheshire.core :as cheshire]
            [cheshire.core :as cheshire]
            [taoensso.timbre :as timbre :refer [spy]]))

;;-----------------------------------------------------------;;
;;-----------------------------------------------------------;;


(defn decode [msg]
  (let [payload (-> msg :payload)]
    (if (bytes? payload)
      ((fn [^bytes payload]
         (cheshire.core/parse-string
          (String. payload "UTF-8") true )) payload)
      ((fn [payload]
         (cheshire.core/parse-string
          payload true)) payload))))

;;-----------------------------------------------------------;;
;; Reader
;;-----------------------------------------------------------;;
(async/put! (async/chan 1) "3rt")

(defn reader
  [{{bind? :bind? exchange :name} :exchange
    conn-info                     :conn-info
    {queue-name         :name
     queue-declare?     :declare?
     queue-rk :routing-key}       :queue
    decoder                       :decoder
    :as config}]
(timbre/spy :info decoder)
  (let
      [conn       (rmq/connect conn-info)
       conn-ch    (lch/open conn)
       _          (timbre/spy :info "[Connection established ...]")
       reader-ch  (async/chan 1)
       close-ch   (async/chan 1)

       handler    (fn subscriber
                    [conn-ch metadata ^bytes payload]
                    (let [msg (timbre/spy (decoder {:meta    metadata
                                                    :payload payload}))]
                       (async/put! reader-ch msg)))

       _          (lq/declare conn-ch
                              queue-name
                              {:exclusive   false
                               :auto-delete true})
       _          (lq/bind conn-ch
                           queue-name
                           exchange
                           {:routing-key queue-rk})
       close-all  (fn []
                    (async/close! close-ch)
                    (async/close! reader-ch))
       subscriber (lc/subscribe conn-ch
                                queue-name
                                handler
                                {:auto-ack true})]
    (try
      (async/go-loop []
        (when-let [_ (async/<! close-ch)]
          (lb/cancel conn-ch subscriber)
          (close-all)))
      {:reader-ch reader-ch
       :close-ch  close-ch}
      (catch Exception e
        (println "[READER ] Some Error!!")
        (close-all)
        (throw e)))))

