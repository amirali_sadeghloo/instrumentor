(ns stellar.instrumentor.components.instrument.core
  (:require
   [com.stuartsierra.component :as component]
   [taoensso.timbre :as timbre :refer [spy]]
   [clojure.core.async :as async]

   [stellar.lib.schema.bixo.transform.core :as brore]
   [stellar.lib.schema.stell.model.rlc     :as smerlc]
   [stellar.lib.schema.stell.model.request :as smequest]

   [stellar.instrumentor.components.db.protocol :as dbtocol])
  (:import (java.util.concurrent Callable ExecutorService Executors)))
;; (:import
;;  (java.util.concurrent.Callable)
;;  (java.util.concurrent.ExecutorService)
;;  (java.util.concurrent Executors))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;

(defn- persistor-fn
  ^Callable
  [db instrument]
  (fn []
    (try (dbtocol/->instrument db instrument)
         (catch Exception e
           (timbre/spy :error ["Instrumenter :: Error while persisting instrument" instrument])
           (.printStackTrace e)))))

(defn- setup-instrumentor-*!
  [^java.util.concurrent.ExecutorService pool
   reader-ch db]
  (async/go
    (loop []
      (if-let [instrument (async/<! reader-ch)]
        (do
          (timbre/spy :info instrument)
          (.submit pool (persistor-fn db instrument))
          (recur))
        (do
          (timbre/spy :error ["instrumenter :: Channel Closed"]))))))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;

(defrecord Instrumentor
    [amqp

     db
     instrumentor-config]

  ;;----------------------------------------------------------------;;
  component/Lifecycle
  ;;----------------------------------------------------------------;;
  (start [component]
    (timbre/spy :info ["Starting Instrumentor ..."])

    (try
      (let [pool-size  (-> instrumentor-config :db-pool-size)
            pool   (Executors/newFixedThreadPool pool-size)
            reader-ch   (-> amqp :instrument-reader :reader-ch)]

        (setup-instrumentor-*! pool reader-ch db)

        (timbre/spy :info ["Instrumentor started"])
        (-> component))

      (catch Exception e
        (timbre/spy :error ["Instrumentor :: Error while starting dumping" ])
        ;; (.printStackTrace e)
        (-> component))))
  ;;----------------------------------------------------------------;;

  (stop [component]
    (timbre/spy :info ["Stopping Instrumenter ..."])))

;;------------------------------------------------------------------;;

(defn subsystem
  []
  (component/system-map
   :instrumentor (-> (map->Instrumentor {})
                     (component/using {:amqp :amqp})
                     (component/using {:db :db})
                     (component/using {:instrumentor-config
                                       :instrumentor-config}))))

;;------------------------------------------------------------------;;
;;------------------------------------------------------------------;;
