(ns stellar.instrumentor.components.instrument.config
  (:require [com.stuartsierra.component :as component]))

(defn subsystem
  [instrumentor-config]
  (component/system-map
   :instrumentor-config instrumentor-config))


