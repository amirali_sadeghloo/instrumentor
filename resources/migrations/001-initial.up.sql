CREATE TABLE "instruments" (

  "instrument-id"                 serial                    PRIMARY KEY,
  "type"                          VARCHAR(128),
  "isin"                          VARCHAR(256) NOT NULL,
  "instrument-name"               VARCHAR(256),
  "english-name"                  VARCHAR(256),
  "description"                   TEXT,
  "tse-id"                        NUMERIC,
  "state"                         TEXT,
  "market-name"                   TEXT,
  "market-code"                   TEXT,
  "num-of-paths-insert"           INTEGER,
  "num-of-paths-ins"              TEXT,
  "num-of-paths"                  INTEGER,
  "instrument-ins"                VARCHAR(256),
  "trading-status-time-table-ins" VARCHAR(256),
  "free-float"                    FLOAT(1),
  "max-order-count"               NUMERIC,
  "min-order-count"               NUMERIC,
  "total-shares"                  NUMERIC,
  "base-volume"                   NUMERIC,
  "validity"                      VARCHAR(256),
  "sector"                        TEXT,
  "sub-sector"                    TEXT,
  "board"                         TEXT,
  "marketUnit"                    TEXT,
  "marketType"                    TEXT,
  "updated-at"                    timestamp,
  "created-at"                    timestamp,
  "instrument-timestamp"          timestamp       default current_timestamp,
  "schema-time-instrument"        timestamp

);

